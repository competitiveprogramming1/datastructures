#pragma "once"

#include <bits/stdc++.h>

#define all(x) begin(x), end(x)
using ll = long long;
using ull = unsigned long long;
using namespace std;

template<typename A, typename B>
string to_string(pair<A,B> p) {
    return "(" + to_string(p.first) + ", " 
    + to_string(p.second) + ")";
}

template<typename A, typename B, typename C>
string to_string(tuple<A,B,C> p) {
    return "(" + to_string(get<0>(p)) + ", " 
    + to_string(get<1>(p)) + ", " 
    + to_string(get<2>(p)) + ")";
}

template<typename A, typename B, typename C, typename D> 
string to_string(tuple<A,B,C,D> p) {
    return "(" + to_string(get<0>(p)) + ", "
    + to_string(get<1>(p)) + ", "
    + to_string(get<2>(p)) + ", "
    + to_string(get<3>(p)) + ")";
}

string to_string(const string& s) {
    return '"' + s + '"';
}

string to_string(const char* s) {
    return to_string((string) s);
}

string to_string(bool s) {
    return (s ? "true" : "false");
}

string to_string(vector<bool>& vec) {
    bool first = true;
    string str = "{";

    for (int i = 0; i < static_cast<int>(vec.size()); i++) {
        if (!first) {
            str += ", ";
        }
        first = false;
        str += to_string(vec[i]);
    }
    str += "}";
    return str;
}

template <size_t N>
string to_string(bitset<N>bs) {
    string str = "";
    for (size_t i = 0; i < N; i++) {
        res += static_cast<char>(bs[i] + '0');
    }
    return str;
}

template <typename A>
string to_string(A o) {
    bool first = true;
    string str = "{";
    for (const auto& x: o) {
        if (!first) {
            str += ", ";
        }
        first = false;
        str += to_string(x);
    }
    str += "}";
    return str;
}

void debug_out() {
    cerr << endl;
}

template<typename Head, typename... Tail>
void debug_out(Head H, Tail... T) {
    cerr << " " << to_string(H);
    debug_out(T...);
}

#ifdef LOCAL
#define debug(...) cerr << "[" << #__VA_ARGS__ << "]:", debug_out(__VA_ARGS__)
#else
#define debug(...) 42
#endif
