#pragma once

#include <stdlib.h>
#include <string>
#include <vector>

using namespace std;

class BigInt {
private:
    bool sign_; // non-negative
    vector<int>arr;
    int BASE_;

public:
    BigInt(const std::string&); 
    BigInt(long long);
    BigInt(const BigInt&);
    BigInt();

    bool get_sign() {
        return sign_;
    }

    std::string to_string() const;
    
    //Direct assignment
    BigInt &operator=(const BigInt &);
    BigInt &operator=(std::string);
    BigInt &operator=(long long);

    friend BigInt &operator+=(BigInt &, const BigInt &);
    friend BigInt operator+(const BigInt &, const BigInt &);
    friend BigInt operator-(const BigInt &, const BigInt &);
    friend BigInt &operator-=(BigInt &, const BigInt &);

    friend BigInt &operator*=(BigInt &, const BigInt &);
    friend BigInt operator*(const BigInt &, const BigInt &);
    friend BigInt &operator/=(BigInt &, const BigInt &);
    friend BigInt operator/(const BigInt &, const BigInt &);

    friend BigInt operator%(const BigInt &, const BigInt &);
    friend BigInt &operator%=(BigInt &, const BigInt &);

    BigInt& operator++(); //pre-inc
    BigInt& operator--(); //pre-dec
    BigInt operator++(int); //post-inc
    BigInt operator--(int); //post-dec


    //Comparison operators
    friend bool operator<(const BigInt&, const BigInt&);
    friend bool operator>(const BigInt&, const BigInt&);
    friend bool operator<=(const BigInt&, const BigInt&);
    friend bool operator>=(const BigInt&, const BigInt&);
    friend bool operator==(const BigInt&, const BigInt&);
    friend bool operator!=(const BigInt&, const BigInt&);

    friend size_t Len(const BigInt&);
    friend bool cabs(const BigInt&, const BigInt&);
    friend bool Null(const BigInt&);
    void to_base(int);

    //input / output
    friend ostream& operator<<(ostream&, const BigInt&);
    friend istream& operator>>(istream&, BigInt&);
};