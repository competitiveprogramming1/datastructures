#include "bigint.hpp"
#include <iostream>
#include <assert.h>

void test_init_from_num() {
    BigInt t = 150;
    assert(t.to_string() == "150");
    t = -800;
    assert(t.to_string() == "-800");
    t = 1;
    assert(t.to_string() == "1");
    t = 0;
    assert(t.to_string() == "0");
}

void test_init_from_string() {
    BigInt t1 = BigInt("150");
    assert(t1.to_string() == "150");
    t1 = "-800";
    assert(t1.to_string() == "-800");
    t1 = "0";
    assert(t1 == 0);
    assert(t1.to_string() == "0");
}

void test_big_nums() {
    BigInt t("999888777666555444333222111000999888000999888000999888000999888000999888000999888");
    assert(t.to_string() == "999888777666555444333222111000999888000999888000999888000999888000999888000999888");
    t = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001";
    assert(t == 1);
}

void test_add() {
    BigInt t1, t2;
    t1 = "450";
    t2 = 550;
    t1 += t2;
    assert(t1.to_string() == "1000");
    t1 = "-85000";
    t2 = "-557";
    t2 += t1;
    std::cout << t2 << '\n';
    assert(t2.to_string() == "-85557");
}

void test_sub() {

}

void test_div() {

}

void test_mul() {
    BigInt t1, t2;
    t1 = 0;
    t2 = 588999;
    t1 *= t2;
    assert(t1.to_string() == "0");
    t1 = "588999";
    t1 *= t2;
    assert(t1.to_string() == "346919822001");
    t1 *= t1;
    assert(t1.to_string() == "120353362897205523644001");
    t1 *= t1;
    assert(t1.to_string() == "14484931960666447156029774620030037449783288001");
    t1 *= t1;
    assert(t1.to_string() == "209813253905136325020497730022320065990406571593099887236497785525550152788378238990510576001");
    t1 *= 0;
    assert(t1.to_string() == "0");
}


int main() {

    test_init_from_num();
    test_init_from_string();
    test_big_nums();
    //test_add();
    test_mul();
    return 0;
}