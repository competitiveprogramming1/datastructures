#include "bigint.hpp"
#include <string>
#include <iostream>
#include <cassert>

BigInt::BigInt() : sign_(true), BASE_(10) {
    arr.push_back(0);
}

BigInt::BigInt(const std::string& str) {
    int sz = str.size();
    
    // reverse order
    for (int i = sz-1 ; i >= 0 + static_cast<int>(str[0] == '-'); i--) {
        if (!isdigit(str[i])) {
            throw("error");
        }
        arr.push_back(str[i] - '0');
    }

    this->BASE_ = 10;
    this->sign_ = (str[0] == '-') ? false : true;
}

BigInt::BigInt(long long num) {
    bool sign = (num < 0) ? false : true;
    if (!sign) {
        num *= -1;
    }
    do {
        arr.push_back(num % 10);
        num /= 10;
    } while (num > 0);
    this->BASE_ = 10;
    this->sign_ = sign;
}

BigInt::BigInt(const BigInt& obj) {
    this->arr = obj.arr;
    this->BASE_ = obj.BASE_;
    this->sign_ = obj.sign_;
}

BigInt& BigInt::operator=(const BigInt & obj) {
    this->arr = obj.arr;
    this->BASE_ = obj.BASE_;
    this->sign_ = obj.sign_;
    return *this;
}

BigInt& BigInt::operator=(std::string s) {
    int sz = s.size();
    // reverse order
    arr.clear();
    for (int i = sz-1 ; i >= 0 + static_cast<int>(s[0] == '-'); i--) {
        if (!isdigit(s[i])) {
            throw("error");
        }
        arr.push_back(s[i] - '0');
    }
    for (int i = arr.size()-1; i >= 1 && !arr[i]; --i) {
        arr.pop_back();
    }

    this->BASE_ = 10;
    this->sign_ = (s[0] == '-') ? false : true;
    return *this;
}

BigInt& BigInt::operator=(long long num) {
    bool sign = (num < 0) ? false : true;
    if (!sign) {
        num *= -1;
    }
    arr.clear();
    do {
        arr.push_back(num % 10);
        num /= 10;
    } while (num > 0);
    this->BASE_ = 10;
    this->sign_ = sign;
    return *this;
}

std::string BigInt::to_string() const {
    // for base = 10 :(
    std::string str = "";
    str.reserve(arr.size());
    if (!sign_) {
        str += '-';
    }
    for (int i = arr.size()-1; i >= 0; i--) {
        str += (arr[i] + '0');
    }
    return str;
}

bool operator<(const BigInt& obj1, const BigInt& obj2) {
    if (obj1.sign_ == obj2.sign_) {
        int ln_1 = Len(obj1);
        int ln_2 = Len(obj2);
        if (ln_1 != ln_2) 
            return ln_1 < ln_2;
        while (ln_1--) {
            if (obj1.arr[ln_1] != obj2.arr[ln_1]) {
                return obj1.arr[ln_1] < obj2.arr[ln_1];
            }
        }
        return false;
    }
    return !obj1.sign_;
}
bool operator>(const BigInt& obj1, const BigInt& obj2) {
    return !(obj1 < obj2 || obj1 == obj2);
}
bool operator<=(const BigInt& obj1, const BigInt& obj2) {
    return !(obj1 > obj2);
}
bool operator>=(const BigInt& obj1, const BigInt& obj2) {
    return !(obj1 < obj2);
}
bool operator==(const BigInt& obj1, const BigInt& obj2) {
    return obj1.sign_ == obj2.sign_ && 
           obj1.arr == obj2.arr;
}
bool operator!=(const BigInt& obj1, const BigInt& obj2) {
    return !(obj1 == obj2);
}

size_t Len(const BigInt& o) {
    return o.arr.size();
}

ostream& operator<<(ostream& os, const BigInt& o) {
    os << o.to_string();
    return os;
}
istream& operator>>(istream& is, BigInt& o) {
    string s;
    is >> s;
    int sz = s.size();
    o.arr.clear();
    for (int i = sz-1; i  >= 0 + (s[0] == '-'); i--) {
        o.arr[sz-i-1] = s[i];
    }
    o.sign_ = (s[0] == '-') ? false : true;
    return is;
}

BigInt& BigInt::operator++() {
    if (this->to_string() == "-1") {
        sign_ = true;
        arr.clear();
        arr.push_back(0);
    } else {
        int i, n = arr.size();
        if (sign_) { // positive number
            for (i = 0; i < n && arr[i] == 9; i++) {
                arr[i] = 0;
            }
            if (i == n) {
                arr.push_back(1);
            } else {
                arr[i]++;
            }
        } else {    //negative number < -1
            --(*this);
        }
        
    }

    return *this;
}
BigInt& BigInt::operator--() {
    if (this->to_string() == "0") {
        sign_ = true;
        arr.clear();
        arr.push_back(1);
    } else {
        int i, n = arr.size();
        if (sign_) { // positive > 0
            for (i = 0; arr[i] == 0 && i < n; i++) {
                arr[i] = 9;
            }
            arr[i]--;
            if (n > 1 && arr[n-1] == 0) {
                arr.pop_back();
            }
        } else { // negative
            ++(*this);
        }
    }
    return *this;
}
BigInt BigInt::operator++(int t) {
    BigInt post = *this;
    ++(*this);
    return post;
}
BigInt BigInt::operator--(int t) {
    BigInt post = *this;
    --(*this);
    return post;
}

BigInt operator%(const BigInt& obj1, const BigInt& obj2) {
    BigInt temp = obj1;
    temp %= obj2;
    return temp;
}

BigInt &operator%=(BigInt &, const BigInt &) {

}

BigInt &operator*=(BigInt& obj1, const BigInt& obj2) {
    if (Null(obj1) || Null(obj2)) {
        obj1 = BigInt();
        return obj1;
    }
    int ln_1 = obj1.arr.size();
    int ln_2 = obj2.arr.size();
    vector<int>vi(ln_1 + ln_2, 0);
    for (int i = 0; i < ln_1; i++) {
        for (int j = 0; j < ln_2; j++) {
            vi[i + j] += (obj1.arr[i] * obj2.arr[j]);
        }
    }
    ln_1 += ln_2;
    obj1.arr.resize(vi.size());
    int carry = 0;
    for (int s, i = 0; i < ln_1; i++) {
        s = carry + vi[i];
        vi[i] = s % 10;
        carry = s / 10;
        obj1.arr[i] = vi[i];
    }
    for (int i = ln_1-1; i >= 1 && !vi[i]; --i) 
        obj1.arr.pop_back();
    
    obj1.sign_ = (obj1.sign_ ^ obj2.sign_) ? false : true;
    return obj1;
}

BigInt operator*(const BigInt& obj1, const BigInt& obj2) {
    BigInt temp = obj1;
    temp *= obj2;
    return temp;
}

BigInt &operator/=(BigInt &, const BigInt &) {

}

BigInt operator/(const BigInt& obj1, const BigInt& obj2) {
    BigInt temp = obj1;
    temp /= obj2;
    return temp;
}

BigInt &operator+=(BigInt& obj1, const BigInt& obj2) {
    int s = 0;
    int r,i;
    if (!(obj1.sign_ ^ obj2.sign_)) { // same signs
        int ln_1 = Len(obj1);
        int ln_2 = Len(obj2);
        if (ln_2 > ln_1)
            obj1.arr.insert(obj1.arr.end(), (ln_2 - ln_1), 0);
        ln_1 = Len(obj1);

        for (i = 0; i < ln_1; i++) {
            s = (i < ln_2) ? (obj1.arr[i] + obj2.arr[i] + r) : (obj1.arr[i] + r);
            r = s / 10;
            obj1.arr[i] = s % 10;
        }
        if (r)
            obj1.arr.push_back(r);
    } else {  //different signs
        //TODO
    }
    return obj1;
}

BigInt operator+(const BigInt& obj1, const BigInt& obj2) {
    BigInt temp = obj1;
    temp += obj2;
    return temp;
}

BigInt operator-(const BigInt& obj1, const BigInt& obj2) {
    BigInt temp = obj1;
    temp -= obj2;
    return temp;
}

BigInt &operator-=(BigInt& obj1, const BigInt& obj2) {
    
}

bool cabs(const BigInt& obj1, const BigInt& obj2) {

}

bool Null(const BigInt& obj) {
    return obj.arr.size() == 1 && obj.arr[0] == 0;
}































